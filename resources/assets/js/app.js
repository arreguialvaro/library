require('jquery');
require('bootstrap-sass');


$('#available').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var bookId = button.data('book_id');
	var bookName = button.data('book_name');
	var modal = $(this);
	modal.find('.modal-title').text('Change the status of the book ' + bookName + '?');
	modal.find('.modal-footer input').val(bookId)
});