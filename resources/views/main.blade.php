<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="google" content="notranslate">
	<meta http-equiv="Content-Language" content="es">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Library</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="{{ asset('css/app.css') }}">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">Library</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/books">Books</a></li>
				</ul>
				{{Form::open(['url' => 'books/search', 'method' => 'post', 'class' => 'navbar-form navbar-right'])}}

					<div class="form-group">
						{{Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Search'])}}
					</div>
					{{Form::submit('Search', ['class' => 'btn btn-default'])}}
				{{Form::close()}}
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>

	<main>
		@yield('content')
	</main>

<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
