@extends('main')
@section('content')
	<div class="container">
		@include('_partials.messages')

		<div>
			<a href="/books/create" class="btn btn-primary">Add new book</a>
		</div>

		<table class="table table-striped">
			<thead>
			<tr>
				<th>Name</th>
				<th>Author</th>
				<th>Category</th>
				<th>Published Date</th>
				<th>User</th>
				<th>Status</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			@foreach($books as $book)
				<tr>
					<td>{{$book->name}}</td>
					<td>{{$book->author}}</td>
					<td>{{$book->category->name}}</td>
					<td>{{$book->published_at}}</td>
					<td>
						@if($book->user)
							{{$book->user->name}}
						@endif
					</td>
					<td>
						<a href="#" class="label <?php echo ($book->available == 1) ? 'label-success' : 'label-warning' ?>"
						   data-toggle="modal" data-target="#available"
						   data-book_id="{{$book->id}}"
						   data-book_name="{{$book->name}}"
						><?php echo ($book->available == 1) ? 'Available' : 'Unavailable' ?></a>


					</td>
					<td>
						<a href="/books/{{$book->id}}/edit" class="btn btn-sm btn-primary">Edit</a>
						<a href="/books/{{$book->id}}/delete" class="btn btn-sm btn-danger">Delete</a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		@if($paginate == true)
			<div class="text-center">
				{{ $books->links() }}
			</div>
		@endif
	</div>
	<div class="modal fade" id="available" tabindex="-1" role="dialog" aria-labelledby="available">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modalTitle">New message</h4>
				</div>
				{{Form::open(['url' => 'books/availability', 'method' => 'post'])}}
					<div class="modal-footer">
							{{Form::hidden('id', null, ['id' => 'bookId'])}}
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


						<button type="submit" class="btn btn-primary">Change</button>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@endsection