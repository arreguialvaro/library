@extends('main')
@section('content')
	<div class="container">
		@include('_partials.messages')
		{{Form::open(['url' => 'books', 'mothod' => 'post'])}}
			<div class="form-group{!! ($errors->has('name')) ? ' has-error' : '' !!}">
				{{Form::label('name', 'Name')}}
				{{Form::text('name', null, ['class' => 'form-control'])}}
			</div>

			<div class="form-group{!! ($errors->has('author')) ? ' has-error' : '' !!}">
				{{Form::label('author', 'Author')}}
				{{Form::text('author', null, ['class' => 'form-control'])}}
			</div>

			<div class="form-group{!! ($errors->has('category_id')) ? ' has-error' : '' !!}">
				{{Form::label('category_id', 'Category')}}
				{{Form::select('category_id', $categories, null, ['class' => 'form-control'])}}
			</div>

			<div class="form-group{!! ($errors->has('published_at')) ? ' has-error' : '' !!}">
				{{Form::label('published_at', 'Published Date')}}
				{{Form::date('published_at', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
			</div>

			<div class="form-group">
				<label>Available</label>
				<div class="radio">
					<label>
						{{Form::radio('available', 1, true)}}
						Yes
					</label>
				</div>
				<div class="radio">
					<label>
						{{Form::radio('available', 0, false)}}
						No
					</label>
				</div>
			</div>

			{{Form::submit('Create', ['class' => 'btn btn-primary'])}}
		{{Form::close()}}
	</div>
@endsection