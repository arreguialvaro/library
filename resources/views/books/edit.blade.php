@extends('main')
@section('content')
	<div class="container">
		@include('_partials.messages')
		{{Form::open(['url' => 'books', 'mothod' => 'put'])}}
		<div class="form-group{!! ($errors->has('name')) ? ' has-error' : '' !!}">
			{{Form::label('name', 'Name')}}
			{{Form::text('name', $book->name, ['class' => 'form-control'])}}
		</div>

		<div class="form-group{!! ($errors->has('author')) ? ' has-error' : '' !!}">
			{{Form::label('author', 'Author')}}
			{{Form::text('author', $book->author, ['class' => 'form-control'])}}
		</div>

		<div class="form-group{!! ($errors->has('category_id')) ? ' has-error' : '' !!}">
			{{Form::label('category_id', 'Category')}}
			{{Form::select('category_id', $categories, $book->category_id, ['class' => 'form-control'])}}
		</div>

		<div class="form-group{!! ($errors->has('published_at')) ? ' has-error' : '' !!}">
			{{Form::label('published_at', 'Published Date')}}
			{{Form::date('published_at', $book->published_at, ['class' => 'form-control'])}}
		</div>

		<div class="form-group">
			<label>Available</label>
			<div class="radio">
				<label>
					{{Form::radio('available', 1, $book->available == 1)}}
					Yes
				</label>
			</div>
			<div class="radio">
				<label>
					{{Form::radio('available', 0, $book->available == 0)}}
					No
				</label>
			</div>
		</div>

		{{Form::submit('Update', ['class' => 'btn btn-primary'])}}
		{{Form::close()}}
	</div>
@endsection