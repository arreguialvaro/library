<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10)
    ];
});


$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->unique()->name,
		'description' => $faker->paragraph(3,3)
	];
});

$factory->define(App\Models\Book::class, function (Faker\Generator $faker) {
	$categories = App\Models\Category::all()->pluck('id')->toArray();
	$users = App\Models\User::all()->pluck('id')->toArray();

	return [
		'name' => $faker->unique()->name,
		'author' => $faker->name,
		'category_id' => $faker->randomElement($categories),
		'user_id' => (rand(0,1) == 1) ? $faker->randomElement($users) : null,
		'published_at' => $faker->date('Y-m-d', 'now'),
		'available' => rand(0,1)
	];
});