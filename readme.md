# Maniak Library

Simple RESTful service for a library. View, create, update and delete books.

##Quickstart

Go to your project directory

```shell
cd /absolute/path/to/library
```

Install PHP dependecies
```shell
composer install
```

Create your enviroment variables and your `APP_KEY`
```shell
cp .env.example
php artisan key:generate
```

After setting up your database credentials in your `.env` file, create the tables needed for the project
```shell
php artisan migrate
```

Fill the tables with some dummy data
```shell
php artisan db:seed
```
This will create 5 users, 8 categories and 100 books

Run Laravels built-in server
```shell
php artisan serve
```

Navigate to [http://localhost:8000](http://localhost:8000)


The CSS & JS files are ready for production. If you need the un-minified/un-compressed versions you can use:
```shell
npm install
npm run dev
```