<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index()
	{
		$books = Book::paginate(10);

		return view('books.index', ['books' => $books, 'paginate' => true]);
	}

	public function create()
	{
		$categories = Category::all()->pluck('name', 'id');

		return view('books.create', ['categories' => $categories]);
	}

	public function store(BookRequest $request)
	{
		$book = Book::create($request->all());

		return redirect('books/'.$book->id.'/edit')->withFlashSuccess("Book created successfully.");
	}

	public function edit($id)
	{
		$book = Book::findOrFail($id);
		$categories = Category::all()->pluck('name', 'id');

		return view('books.edit', ['book' => $book, 'categories' => $categories]);
	}

	public function update(BookRequest $request, $id)
	{
		$book = Book::findOrFail($id);
		$book->update($request->all());

		return redirect('books/'.$book->id.'/edit')->withFlashSuccess("Book updated successfully.");
	}

	public function search(Request $request)
	{
		$this->validate($request, [
			'search' => 'required|string|min:3'
		]);
		$books = Book::search($request->search)->paginate(10);

		\Session::flash('flash_success', "Se encontraron {$books->count()} resultados.");
		return view('books.index', ['books' => $books, 'paginate' => false]);
	}

	public function delete($id)
	{
		$book = Book::findOrFail($id);

		$book->delete();

		return redirect('books')->withFlashSuccess("Book deleted successfully.");
	}

	public function availability(Request $request)
	{
		$this->validate($request,[
			'id' => 'required|exists:books,id'
		]);

		$book = Book::find($request->id);
		$book->available = !$book->available;
		$book->save();

		return redirect('books')->withFlashSuccess("Book updated successfully.");
	}
}
