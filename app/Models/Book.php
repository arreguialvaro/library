<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

	public $timestamps = false;
	protected $fillable = [
		'name',
		'author',
		'category_id',
		'published_at',
		'available'
	];

	public function category()
	{
		return $this->hasOne(Category::class, 'id', 'category_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	public function scopeSearch($query, $search)
	{
		return $query->where('name', 'like', "%{$search}%")
			->orWhere('author', 'like', "%{$search}%");
	}
}
